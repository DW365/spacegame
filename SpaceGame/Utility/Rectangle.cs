﻿namespace SpaceGame.Utility
{
    struct Rectangle
    {
        public int X1;
        public int Y1;
        public int X2;
        public int Y2;
        public Rectangle(int x1, int y1, int x2, int y2)
        {
            X1 = x1;
            Y1 = y1;
            X2 = x2;
            Y2 = y2;
        }
        public Rectangle(Point a, Point b)
        {
            X1 = a.X;
            Y1 = a.Y;
            X2 = b.X;
            Y2 = b.Y;
        }
    }
}
