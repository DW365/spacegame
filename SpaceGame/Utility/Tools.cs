﻿using System;

namespace SpaceGame.Utility
{
    static class Tools
    {
        /// <summary>
        /// Проводит первичную инициализацию консоли.
        /// </summary>
        public static void ConsoleInit()
        {
            Console.CursorVisible = false;
            Console.Title = "Space Game";
            Console.SetWindowPosition(0, 0);
            Console.SetWindowSize(Globals.SCREEN_WIDTH, Globals.SCREEN_HEIGHT);
            Console.SetBufferSize(Globals.SCREEN_WIDTH, Globals.SCREEN_HEIGHT);
        }
        public static bool Intersect(Point point, Rectangle rect)
        {
            if (point.X >= rect.X1 && point.X <= rect.X2 && point.Y >= rect.Y1 && point.Y <= rect.Y2) return true;
            return false;
        }
        public static bool Intersect(Rectangle rect1, Rectangle rect2)
        {
            Point[] ponts = {new Point(rect1.X1, rect1.Y1),
                             new Point(rect1.X2, rect1.Y2),
                             new Point(rect1.X1, rect1.Y2),
                             new Point(rect1.X2, rect1.Y1)};
            foreach(var point in ponts)
            {
                if (Intersect(point, rect2)) return true;
            }
            return false;
        }
    }
}
