﻿using System;

namespace SpaceGame.Utility
{
    static class Debug
    {      
        public static void Log(string message)
        {
            System.Diagnostics.Debug.WriteLine(message);
        }
        public static void Log(Point point)
        {
            System.Diagnostics.Debug.WriteLine(String.Format("X:{0} Y:{1}", point.X,point.Y));
        }
    }
}
