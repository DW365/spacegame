﻿using System;
using System.Collections.Generic;

namespace SpaceGame.Utility
{
    class GameSprite
    {
        private List<string> Sprite { get; set; }
        private ConsoleColor Color { get; set; }
        public int Width {get; private set;}
        public int Height { get; private set; }
        public GameSprite(string filename)
        {
            Sprite = new List<string>(System.IO.File.ReadAllLines(@"Sprites\" + filename));
            Height = Sprite.Count;
            Width = 0;
            foreach(var line in Sprite)
            {
                Width = Math.Max(Width,line.Length);
                Debug.Log(line);
            }
        }
        public GameSprite(string filename, ConsoleColor color)
            : this(filename)
        {
            Color = color;
        }
        public void Show(Point position)
        {
            Console.ForegroundColor = Color;
            for(int i = 0; i < Sprite.Count; i++)
            {
                Console.SetCursorPosition(position.X,position.Y+i);
                Console.Write(Sprite[i]);
            }
        }
    }
}
