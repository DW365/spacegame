﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceGame.Utility
{
    class Timer
    {
        private int maxDelay;
        private int currentDelay;
        public Timer(int time)
        {
            maxDelay = time;
            currentDelay = time;
        }
        public bool Step()
        {
            if(currentDelay == 0)
            {
                currentDelay = maxDelay;
                return true;
            }
            currentDelay--;
            return false;
        }
        public override string ToString()
        {
            return String.Format("Timer {0}|{1}",maxDelay,currentDelay);
        }
    }
}
