﻿using System;

namespace SpaceGame
{
    static class Globals
    {
        public const int SCREEN_WIDTH = 80;
        public const int SCREEN_HEIGHT = 25;
        public const int DEFAULT_MENU_WIDTH = 20;
        public const int LIVES = 3;
        public const int DELAY = 1600000;
        public const int FIGHTER_DELAY = 3;
        public const int DODGER_DELAY = 1;
        public static readonly Utility.Point START_POS = new Utility.Point(2,10);
        public static readonly Random RandomGenerator = new Random();
    }
}
