﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpaceGame.GameObjects;

namespace SpaceGame.Levels
{
    class Level5 : Level
    {
        Utility.Timer DelayToCreate;
        public Level5()
            : base()
        {
            DelayToCreate = new Utility.Timer(60);
        }
        protected override void UpdateLogic()
        {
            if (DelayToCreate.Step())
            {
                var maxHealth = Score / 50;
                if (Entities.FindAll(x => x is Enemy).Count < 5 + maxHealth)
                {
                    switch (Globals.RandomGenerator.Next(5))
                    {
                        case 0: Add(new GameObjects.Enemies.Fighter(new Utility.GameSprite("fighter_alt.sprite", ConsoleColor.Yellow), new Utility.Point(75, Globals.RandomGenerator.Next(3, 21)), Globals.RandomGenerator.Next(3, maxHealth + 8)));
                            break;
                        case 1: Add(new GameObjects.Enemies.Fighter(new Utility.GameSprite("fighter_alt.sprite", ConsoleColor.DarkRed), new Utility.Point(75, Globals.RandomGenerator.Next(3, 21)), Globals.RandomGenerator.Next(3, maxHealth + 8)));
                            break;
                        case 2: Add(new GameObjects.Enemies.Fighter(new Utility.GameSprite("fighter.sprite", ConsoleColor.DarkYellow), new Utility.Point(75, Globals.RandomGenerator.Next(3, 21)), Globals.RandomGenerator.Next(1, maxHealth + 5)));
                            break;
                        case 3: Add(new GameObjects.Enemies.Fighter(new Utility.GameSprite("fighter.sprite", ConsoleColor.White), new Utility.Point(75, Globals.RandomGenerator.Next(3, 21)), Globals.RandomGenerator.Next(1, maxHealth + 5)));
                            break;
                        case 4: Add(new GameObjects.Enemies.Fighter(new Utility.GameSprite("fighter_alt.sprite", ConsoleColor.DarkMagenta), new Utility.Point(75, Globals.RandomGenerator.Next(3, 21)), Globals.RandomGenerator.Next(3, maxHealth + 8)));
                            break;
                        default: Add(new GameObjects.Enemies.Fighter(new Utility.GameSprite("fighter.sprite", ConsoleColor.Yellow), new Utility.Point(75, Globals.RandomGenerator.Next(3, 21)), Globals.RandomGenerator.Next(1, maxHealth + 5)));
                            break;
                    }
                }
            }
        }
    }
}
