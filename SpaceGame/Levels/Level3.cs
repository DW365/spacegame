﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpaceGame.GameObjects;

namespace SpaceGame.Levels
{
    class Level3 : Level
    {
        Utility.Timer DelayToCreate;
        public Level3()
            : base()
        {
            Player.Instance.Lives = 8;
            DelayToCreate = new Utility.Timer(30);
        }
        protected override void UpdateLogic()
        {
            if (DelayToCreate.Step())
            {
                if (Entities.FindAll(x => x is Enemy).Count < 6+MaxHealth())
                {
                    switch (Globals.RandomGenerator.Next(7))
                    {
                        case 0: Add(new GameObjects.Enemies.Dodger(new Utility.GameSprite("dodger.sprite", ConsoleColor.Yellow), new Utility.Point(75, Globals.RandomGenerator.Next(3, 21)), Globals.RandomGenerator.Next(3, 8 + MaxHealth())));
                            break;
                        case 1: Add(new GameObjects.Enemies.Dodger(new Utility.GameSprite("dodger.sprite", ConsoleColor.DarkRed), new Utility.Point(75, Globals.RandomGenerator.Next(3, 21)), Globals.RandomGenerator.Next(3, 8 + MaxHealth())));
                            break;
                        case 2: Add(new GameObjects.Enemies.Dodger(new Utility.GameSprite("dodger.sprite", ConsoleColor.DarkYellow), new Utility.Point(75, Globals.RandomGenerator.Next(3, 21)), Globals.RandomGenerator.Next(1, 5 + MaxHealth())));
                            break;
                        case 3: Add(new GameObjects.Enemies.Dodger(new Utility.GameSprite("dodger.sprite", ConsoleColor.White), new Utility.Point(75, Globals.RandomGenerator.Next(3, 21)), Globals.RandomGenerator.Next(1, 5 + MaxHealth())));
                            break;
                        case 4: Add(new GameObjects.Enemies.Dodger(new Utility.GameSprite("dodger.sprite", ConsoleColor.DarkMagenta), new Utility.Point(75, Globals.RandomGenerator.Next(3, 21)), Globals.RandomGenerator.Next(3, 8 + MaxHealth())));
                            break;
                        default: Add(new GameObjects.Enemies.Dodger(new Utility.GameSprite("dodger.sprite", ConsoleColor.Yellow), new Utility.Point(75, Globals.RandomGenerator.Next(3, 21)), Globals.RandomGenerator.Next(1, 5 + MaxHealth())));
                            break;
                    }
                }
            }
        }
    }
}
