﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpaceGame.GameObjects;

namespace SpaceGame.Levels
{
    class Level4 : Level
    {
        Utility.Timer DelayToCreate;
        public Level4()
            : base()
        {
            Player.Instance.Lives = 14;
            DelayToCreate = new Utility.Timer(60);
        }
        protected override void UpdateLogic()
        {
            if (DelayToCreate.Step())
            {
                if (Entities.FindAll(x => x is GameObjects.Enemies.Miniboss).Count < 1 + MaxHealth()/8 && Globals.RandomGenerator.Next(3) == 0)
                {
                    switch (Globals.RandomGenerator.Next(2))
                    {
                        case 0: Add(new GameObjects.Enemies.Miniboss(new Utility.GameSprite("miniboss.sprite", ConsoleColor.Yellow), new Utility.Point(70, Globals.RandomGenerator.Next(3, 20)), Globals.RandomGenerator.Next(15, MaxHealth() + 25)));
                            break;
                        case 1: Add(new GameObjects.Enemies.Miniboss(new Utility.GameSprite("miniboss.sprite", ConsoleColor.DarkRed), new Utility.Point(70, Globals.RandomGenerator.Next(3, 20)), Globals.RandomGenerator.Next(10, MaxHealth() + 15)));
                            break;
                        default: Add(new GameObjects.Enemies.Miniboss(new Utility.GameSprite("fighter.sprite", ConsoleColor.Yellow), new Utility.Point(70, Globals.RandomGenerator.Next(3, 20)), Globals.RandomGenerator.Next(1, MaxHealth() + 5)));
                            break;
                    }
                }
                if (Entities.FindAll(x => x is GameObjects.Enemies.Fighter).Count < 3 + MaxHealth())
                {
                    switch (Globals.RandomGenerator.Next(2))
                    {
                        case 0: Add(new GameObjects.Enemies.Fighter(new Utility.GameSprite("fighter.sprite", ConsoleColor.Yellow), new Utility.Point(70, Globals.RandomGenerator.Next(3, 20)), Globals.RandomGenerator.Next(1, MaxHealth() + 3)));
                            break;
                        case 1: Add(new GameObjects.Enemies.Fighter(new Utility.GameSprite("fighter_alt.sprite", ConsoleColor.DarkRed), new Utility.Point(70, Globals.RandomGenerator.Next(3, 20)), Globals.RandomGenerator.Next(1, MaxHealth() + 2)));
                            break;
                        default: Add(new GameObjects.Enemies.Fighter(new Utility.GameSprite("fighter.sprite", ConsoleColor.Yellow), new Utility.Point(70, Globals.RandomGenerator.Next(3, 20)), Globals.RandomGenerator.Next(1, MaxHealth() + 5)));
                            break;
                    }
                }
            }
        }
    }
}
