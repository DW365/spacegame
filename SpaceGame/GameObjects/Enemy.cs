﻿using System;
using System.Collections.Generic;
using System.Linq;
using SpaceGame.Utility;
using System.Threading.Tasks;

namespace SpaceGame.GameObjects
{
    abstract class Enemy : GameObject, Damageble, Solid
    {
        protected int Lives { get; set; }
        protected Timer UpdateDelay { get; set; }
        public Enemy(GameSprite sprite, Point position, int lives, int delay)
            : base(sprite, position)
        {
            Lives = lives;
            UpdateDelay = new Timer(delay);
        }
        public virtual void GetDamage(int damage = 1)
        {
            Lives -= damage;
        }
        public virtual bool IsAlive()
        {
            if(Lives <= 0) return false;
            return true;
        }
        protected override void Remove()
        {
            base.Remove();
        }
    }
}
