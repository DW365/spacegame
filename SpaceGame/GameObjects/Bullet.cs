﻿using System;
using System.Collections.Generic;
using SpaceGame.Utility;
using System.Text;
using System.Threading.Tasks;

namespace SpaceGame.GameObjects
{
    class Bullet : GameObject 
    {
        public Bullet(Point position)
            :base(new GameSprite("bullet.sprite",ConsoleColor.Red),position)
        {
            
        }
        protected override void Remove()
        {
 	        Parent.Remove(this);
        }
        public override void Update()
        {
            if (Position.X >= Globals.SCREEN_WIDTH-1) Remove();
            for (int i = 0; i < Parent.Entities.Count; i++)
            {
                var obj = Parent.Entities[i];
                if (Tools.Intersect(Position, new Rectangle(obj.Position, obj.Position + new Point(obj.Sprite.Width, obj.Sprite.Height))))
                {
                    if (obj is Damageble) ((Damageble)obj).GetDamage(1);
                    if (obj is Solid)
                    {
                        Remove();
                        obj.Show();
                        return;
                    }
                }
            }
                Debug.Log(Position);
                var newPosition = Position;
                newPosition += new Point(1, 0);
                if (IsCorrect(newPosition))
                {
                    Position = newPosition;
                }
                Show();
        }
    }
}
