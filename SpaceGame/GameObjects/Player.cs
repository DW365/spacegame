﻿using System;
using System.Collections.Generic;
using SpaceGame.Utility;
using System.Text;
using System.Threading.Tasks;

namespace SpaceGame.GameObjects
{
    class Player : GameObject, Solid, Damageble
    {
        private static readonly Player instance = new Player(new GameSprite("player.sprite",ConsoleColor.Green),new Point(5,10));
        public int Lives { get; set; }
        private int FireDelay { get; set; }
        private int TimerToFire { get; set; }
        private Player(GameSprite sprite, Point position)
            : base(sprite, position)
        {
            Lives = Globals.LIVES;
            TimerToFire = 0;
            FireDelay = 4;
        }

        public void GetDamage(int damage)
        {
            if (Lives == 0) Parent.GameOver();
            Lives--;
            Position = new Point(1, Globals.SCREEN_HEIGHT / 2-2);
        }
        public static Player Instance
        {
            get{ return instance; }
        }
        public void ResetPosition()
        {
            Position = Globals.START_POS;
        }
        private bool CanFire()
        {
            if(TimerToFire == 0)
            {
                TimerToFire = FireDelay;
                return true;
            }
            else
            {
                return false;
            }
        }
        private void Fire()
        {
            if(CanFire())
            {
                if(Parent.Score < 150)
                    Parent.Add(new Bullet(Position + new Point(Sprite.Width + 1, 2)));
                if (Parent.Score >= 150 && Parent.Score < 600)
                {
                    Parent.Add(new Bullet(Position + new Point(Sprite.Width + 1, 1)));
                    Parent.Add(new Bullet(Position + new Point(Sprite.Width + 1, 3)));
                } 
                if (Parent.Score >= 600)
                {
                    Parent.Add(new Bullet(Position + new Point(Sprite.Width + 1, 0)));
                    Parent.Add(new Bullet(Position + new Point(Sprite.Width + 1, 2)));
                    Parent.Add(new Bullet(Position + new Point(Sprite.Width + 1, 4)));
                }  
            }
        }
        
        public override void Update()
        {
            if(Console.KeyAvailable)
            {
                var newPosition = Position;
                var key = Console.ReadKey(true).Key;
                if(key == ConsoleKey.W)
                    newPosition += new Utility.Point(0, -1);
                if (key == ConsoleKey.S)
                    newPosition += new Utility.Point(0, 1);
                if (key == ConsoleKey.A)
                    newPosition += new Utility.Point(-1, 0);
                if (key == ConsoleKey.D)
                    newPosition += new Utility.Point(1, 0);
                if (key == ConsoleKey.Spacebar)
                    Fire();
                if (key == ConsoleKey.P)
                    GUI.GraphicalUserInterface.Instance.ShowPause();
                if (IsCorrect(newPosition)) 
                    Position = newPosition;
                else
                    newPosition = Position;
                Debug.Log(String.Format("{0}:{1} {2}", Position.X, Position.Y, TimerToFire));
            }
            if (TimerToFire > 0) TimerToFire--;
            Show();
        }
    }
}
