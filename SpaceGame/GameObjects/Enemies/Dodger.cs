﻿using System;
using System.Collections.Generic;
using System.Linq;
using SpaceGame.Utility;
using System.Threading.Tasks;

namespace SpaceGame.GameObjects.Enemies
{
    class Dodger : Enemy, Scorable
    {
        public Dodger(GameSprite sprite, Point position, int lives = 4)
            : base(sprite, position, lives, Globals.DODGER_DELAY)
        {
        }
        public int GetScore()
        {
            return 30;
        }
        public override void Update()
        {
            if (UpdateDelay.Step())
            {
                if (!IsAlive()) Remove();
                for (int i = 0; i < Parent.Entities.Count; i++)
                {
                    var obj = Parent.Entities[i];
                    if (obj != this && Tools.Intersect(new Rectangle(Position, Position + new Point(Sprite.Width, Sprite.Height)), new Rectangle(obj.Position, obj.Position + new Point(obj.Sprite.Width, obj.Sprite.Height))))
                    {
                        if (obj is Player)
                        {
                            Remove();
                            Player.Instance.GetDamage(100);
                            return;
                        }
                    }
                }
                var newPosition = Position;
                newPosition += new Utility.Point(-1, Globals.RandomGenerator.Next(-1,2));
                if (IsCorrect(newPosition))
                {
                    Position = newPosition;
                }
                else
                {
                    if(newPosition.X < 0)
                    {
                        Parent.AddScore(-100);
                        Remove();
                    }
                }

            }
            Show();
        }
    }
}
