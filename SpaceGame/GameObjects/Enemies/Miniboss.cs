﻿using System;
using System.Collections.Generic;
using System.Linq;
using SpaceGame.Utility;
using System.Threading.Tasks;

namespace SpaceGame.GameObjects.Enemies
{
    class Miniboss : Enemy, Scorable
    {
        private Utility.Timer TimerToFire;
        public Miniboss(GameSprite sprite, Point position, int lives = 4)
            : base(sprite, position, lives, Globals.FIGHTER_DELAY)
        {
            TimerToFire = new Timer(10);
        }
        public int GetScore()
        {
            return 65;
        }
        void Fire()
        {
            Parent.Add(new EBullet(Position + new Point(-2, 2)));
        }
        public override void Update()
        {
            if (UpdateDelay.Step())
            {
                if(TimerToFire.Step())
                {
                    Fire();
                }
                if (!IsAlive()) Remove();
                for (int i = 0; i < Parent.Entities.Count; i++)
                {
                    var obj = Parent.Entities[i];
                    if (obj != this && Tools.Intersect(new Rectangle(Position, Position + new Point(Sprite.Width, Sprite.Height)), new Rectangle(obj.Position, obj.Position + new Point(obj.Sprite.Width, obj.Sprite.Height))))
                    {
                        if (obj is Player)
                        {
                            Remove();
                            Player.Instance.GetDamage(100);
                            return;
                        }
                    }
                }
                var newPosition = Position;
                newPosition += new Utility.Point(-1, Globals.RandomGenerator.Next(-1, 2));
                if (IsCorrect(newPosition))
                {
                    Position = newPosition;
                }
                else
                {
                    if (newPosition.X < 0)
                    {
                        Parent.AddScore(-100);
                        Remove();
                    }
                }

            }
            Show();
        }
    }
}
