﻿using System;
using System.Collections.Generic;
using System.Linq;
using SpaceGame.Utility;
using System.Threading.Tasks;

namespace SpaceGame.GameObjects.Enemies
{
    class Fighter : Enemy, Scorable
    {
        public Fighter(GameSprite sprite, Point position, int lives = 2)
            :base(sprite,position,lives, Globals.FIGHTER_DELAY)
        {
        }
        public int GetScore()
        {
            return 10;
        }
        public override void Update()
        {
            if (UpdateDelay.Step())
            {
                if (!IsAlive()) Remove();
                for (int i = 0; i < Parent.Entities.Count; i++)
                {
                    var obj = Parent.Entities[i];
                    if (obj != this && Tools.Intersect(new Rectangle(Position, Position + new Point(Sprite.Width, Sprite.Height)), new Rectangle(obj.Position, obj.Position + new Point(obj.Sprite.Width, obj.Sprite.Height))))
                    {
                        if (obj is Player)
                        {
                            Remove();
                            Player.Instance.GetDamage(100);
                            return;
                        }
                    }
                }
                var newPosition = Position;
                newPosition += new Utility.Point(-1, 0);
                if (IsCorrect(newPosition))
                {
                    Position = newPosition;
                }
                else
                {
                    Parent.AddScore(-100);
                    Remove();
                }
                    
            }
            Show();
        }
    }
}
