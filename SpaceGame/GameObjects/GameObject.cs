﻿using System;
using System.Collections.Generic;
using SpaceGame.Utility;
using System.Text;

namespace SpaceGame.GameObjects
{
    abstract class GameObject
    {
        public Point Position { get; protected set; }
        public GameSprite Sprite { get; set; }
        public Level Parent { get; set; }

        public GameObject(GameSprite sprite, Point position)
        {
            Sprite = sprite;
            Position = position;
        }
        protected virtual void Remove()
        {
            Parent.Remove(this);
            for(int y = 0; y < Sprite.Height; y++)
            {
                for(int x = 0; x < Sprite.Width; x++)
                {
                    Debug.Log(String.Format("||{0} {1}||",Position.X+x, Position.Y+y));
                    Console.SetCursorPosition(Position.X+x, Position.Y+y);
                    Console.Write(" ");
                }
            }
        }
        public GameObject(GameSprite sprite, Point position, Level parent)
            :this(sprite,position)
        {
            Parent = parent;
        }
        public virtual void Show()
        {
            Sprite.Show(Position);
        }
        public virtual bool IsCorrect(Point position)
        {
            if (position.X < 0 ||
                position.Y < 0 ||
                position.X > Globals.SCREEN_WIDTH - Sprite.Width ||
                position.Y > Globals.SCREEN_HEIGHT - Sprite.Height) return false;
            return true;
        }
        public abstract void Update();
    }
}
