﻿using System;
using System.Collections.Generic;

namespace SpaceGame.GUI
{
    class Screen
    {
        private int CaptionSize { get; set; }
        List<MenuElement> Buttons { get; set; }
        private int ActiveElement { get; set; }
        private string Text { get; set; }
        ConsoleColor[] colors;
        ConsoleColor[] Colors
        {
            get
            {
                return colors;
            }
            set
            {
                if (value.Length != 4) throw new Exception("Количество цветов не соответсвует четырем");
                colors = value;
            }
        }
        public Screen(string text, List<MenuElement> buttons, ConsoleColor[] colors)
        {
            Text = text;
            Buttons = buttons;
            Colors = colors;
            CaptionSize = Globals.SCREEN_WIDTH-4;
        }
        public void Show()
        {
            ActiveElement = 0;
            Clear();
            DrawScreen();
            foreach(var button in Buttons)
            {
                button.IsActive = false;
                button.Show();
            }
            Buttons[ActiveElement].ChangeState();
            for(;;)
            {
                Console.BackgroundColor = Colors[0];
                var key = Console.ReadKey(true).Key;
                if(key == ConsoleKey.DownArrow)
                {
                    Buttons[ActiveElement].ChangeState();
                    ActiveElement++;
                    ActiveElement = ActiveElement % Buttons.Count;
                    Buttons[ActiveElement].ChangeState();
                }
                if(key == ConsoleKey.UpArrow)
                {
                    Buttons[ActiveElement].ChangeState();
                    ActiveElement--;
                    ActiveElement = (Buttons.Count + (ActiveElement % Buttons.Count)) % Buttons.Count;
                    Buttons[ActiveElement].ChangeState();
                }
                if (key == ConsoleKey.Enter)
                {
                    Buttons[ActiveElement].Click();
                }
            }
        }
        private void Clear()
        {
            Console.BackgroundColor = Colors[0];
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Clear();
            for(int y = 0; y < Globals.SCREEN_HEIGHT; y++)
            {
                for(int x = 0; x < Globals.SCREEN_WIDTH; x++)
                {
                    Console.SetCursorPosition(x, y);
                    if (x != Globals.SCREEN_WIDTH - 1 || y != Globals.SCREEN_HEIGHT - 1) Console.Write("▼");
                }
            }
        }
        private void DrawScreen()
        {
            MenuElement caption = new MenuElement(Text,new Action(Show),new Utility.Point((Globals.SCREEN_WIDTH - CaptionSize)/2,1),new Utility.Point(CaptionSize,5),new ConsoleColor[6] {ConsoleColor.Black,ConsoleColor.Black,ConsoleColor.Black,Colors[1],Colors[2],Colors[3]});
            caption.Show();
        }
    }
}
