﻿using System;
using System.Collections.Generic;

namespace SpaceGame.GUI
{
    sealed class GraphicalUserInterface
    {
        private static readonly GraphicalUserInterface instance = new GraphicalUserInterface();

        Screen Main;
        Screen LevelSelect;
        Screen Settings;
        Screen Pause;

        ConsoleColor[] captionColors { get; set; }
        Utility.Point size { get; set; }
        int x { get; set; }
        ConsoleColor[] colors { get; set; }

        private GraphicalUserInterface()
        {
            size = new Utility.Point(Globals.DEFAULT_MENU_WIDTH, 3);
            x = (Globals.SCREEN_WIDTH - size.X) / 2;

            captionColors = new ConsoleColor[4] { ConsoleColor.DarkGray, 
                                                  ConsoleColor.Black, 
                                                  ConsoleColor.White, 
                                                  ConsoleColor.DarkCyan };

            colors = new ConsoleColor[6] { ConsoleColor.Black, 
                                           ConsoleColor.DarkBlue, 
                                            ConsoleColor.Green, 
                                           ConsoleColor.Black, 
                                           ConsoleColor.DarkCyan, 
                                           ConsoleColor.Blue };
        }
        public static GraphicalUserInterface Instance
        {
            get { return instance; }
        }

        public void Start()
        {
            Init();
            Main.Show();
        }
        private void Init()
        {
            MainInit();
            LevelSelectInit();
            SettingsInit();
            PauseInit();
        }
        private void MainInit()
        {
            var elements = new List<GUI.MenuElement>();
            elements.Add(new GUI.MenuElement("Играть", new Action(ShowLevel), new Utility.Point(x, 4 + size.Y+2), size, colors));
            elements.Add(new GUI.MenuElement("Настройки", new Action(ShowSettings), new Utility.Point(x, 4 + (size.Y + 2) * 2), size, colors));
            elements.Add(new GUI.MenuElement("Выход", new Action(Game.Instance.Exit), new Utility.Point(x, 4 +(size.Y + 2) * 3), size, colors));
            Main = new GUI.Screen("Space Game", elements, captionColors);
        }
        private void LevelSelectInit()
        {
            var elements = new List<GUI.MenuElement>();
            elements.Add(new GUI.MenuElement("Уровень 1", new Action(() => Game.Instance.StartLevel(1)), new Utility.Point(x - x / 2, 10), size, colors));
            elements.Add(new GUI.MenuElement("Уровень 2", new Action(() => Game.Instance.StartLevel(2)), new Utility.Point(x - x / 2, 15), size, colors));
            elements.Add(new GUI.MenuElement("Уровень 3", new Action(() => Game.Instance.StartLevel(3)), new Utility.Point(x - x / 2, 20), size, colors));
            elements.Add(new GUI.MenuElement("Уровень 4", new Action(() => Game.Instance.StartLevel(4)), new Utility.Point(x + x / 2, 10), size, colors));
            elements.Add(new GUI.MenuElement("Уровень 5", new Action(() => Game.Instance.StartLevel(5)), new Utility.Point(x + x / 2, 15), size, colors));
            elements.Add(new GUI.MenuElement("Назад", new Action(ShowMain), new Utility.Point(x + x / 2, 20), size, colors));
            LevelSelect = new GUI.Screen("Выберите уровень", elements, captionColors);
        }
        private void SettingsInit()
        {
            var elements = new List<GUI.MenuElement>();
            elements.Add(new GUI.MenuElement("Назад", new Action(ShowMain), new Utility.Point(x, 10), size, colors));
            Settings = new GUI.Screen("Настройки", elements, captionColors);
        }
        private void PauseInit()
        {
            var elements = new List<GUI.MenuElement>();
            elements.Add(new GUI.MenuElement("Вернуться в игру", new Action(Game.Instance.Resume), new Utility.Point(x, 10), size, colors)); //TODO
            elements.Add(new GUI.MenuElement("Главное меню", new Action(ShowMain), new Utility.Point(x, 15), size, colors));
            elements.Add(new GUI.MenuElement("Выход", new Action(Game.Instance.Exit), new Utility.Point(x, 20), size, colors));
            Pause = new GUI.Screen("Пауза", elements, captionColors);
        }
        public void ShowBattleScreen(int score, int lives)
        {
            Console.Clear();
            Console.SetCursorPosition(0, 0);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Score: {0} Lives: {1}",score, lives);
        }
        public void ShowMain() { Main.Show(); }
        private void ShowLevel() { LevelSelect.Show(); }
        private void ShowSettings() { Settings.Show(); }
        public void ShowPause() { Pause.Show(); } 
    }
}
