﻿using System;
using SpaceGame.Utility;

namespace SpaceGame.GUI
{
    class MenuElement
    {
        //Данные
        public bool IsActive { get; set; }
        private event Action ActionEvent;

        private string text;
        private string Text
        {
            get
            {
                return text;
            }
            set
            {
                if (value.Length > Size.X - 2) throw new Exception("Длина строки превышает длину поля");
                text = value;
            }
        }
        private Point Position { get; set; }

        private Point size;
        private Point Size
        {
            get
            {
                return size;
            }
            set
            {
                if (value.Y < 3) throw new Exception("Высота пункта меню должна быть больше трех");
                size = value;
            }
        }

        private ConsoleColor[] colors;
        private ConsoleColor[] Colors
        {
            get 
            { 
                return colors; 
            }
            set
            {
                if (value.Length != 6) throw new Exception("Количество цветов не соответствует шести");
                colors = value;
            }
        }
        private ConsoleColor BorderColor { get; set; }
        private ConsoleColor BackgroundColor { get; set; }
        private ConsoleColor TextColor { get; set; }
        
        //Функции
        /// <summary>
        /// Конструктор объекта меню, содержащий максимальное количество параметров
        /// </summary>
        /// <param name="text">Отображаемый на элементе текст</param>
        /// <param name="action">Делегат типа Action, вызываемый при активации элемента</param>
        /// <param name="position">Позиция элемента на экране</param>
        public MenuElement(string text, Action action, Point position)
        {
            IsActive = false;
            ActionEvent += action;
            Position = position;
            Size = new Point(Globals.SCREEN_WIDTH / 2, 3);
            Text = text;
            //Colors = Settings.MenuDefaultColors();
        }
        /// <summary>
        /// Конструктор объекта меню
        /// </summary>
        /// <param name="text">Отображаемый на элементе текст</param>
        /// <param name="action">Делегат типа Action, вызываемый при активации элемента</param>
        /// <param name="position">Позиция элемента на экране</param>
        /// <param name="size">Размеры кнопки, высота больше трех, длина не меньше длины текста</param>
        public MenuElement(string text, Action action, Point position, Point size) 
            : this(text, action, position)
        {
            Size = size;
        }
        /// <summary>
        /// Конструктор объекта меню, содержащий максимальное количество параметров
        /// </summary>
        /// <param name="text">Отображаемый на элементе текст</param>
        /// <param name="action">Делегат типа Action, вызываемый при активации элемента</param>
        /// <param name="position">Позиция элемента на экране</param>
        /// <param name="size">Размеры кнопки, высота больше трех, длина не меньше длины текста</param>
        /// <param name="colors">Цвет рамки, бекграунда, текста активной кнопки. Цвет рамки, бекграунда, текста неактивной кнопки</param>
        public MenuElement(string text, Action action, Point position, Point size, ConsoleColor[] colors)
            : this(text, action, position, size)
        {
            Colors = colors;
        }

        public void Click()
        {
            ActionEvent();
        }
        public void ChangeState()
        {
            IsActive = !IsActive;
            UpdateColors();
            Show();
        }
        public void Show()
        {
            UpdateColors();
            DrawBorder();
            Console.ForegroundColor = TextColor;
            Console.BackgroundColor = BackgroundColor;
            Console.SetCursorPosition((Size.X - Text.Length) / 2 + Position.X, Size.Y / 2 + Position.Y);
            Console.Write(Text);
        }
        private void UpdateColors()
        {
            if(IsActive)
            {
                BorderColor = Colors[0];
                BackgroundColor = Colors[1];
                TextColor = Colors[2];
            }
            else
            {
                BorderColor = Colors[3];
                BackgroundColor = Colors[4];
                TextColor = Colors[5];
            }
        }
        private void DrawBorder()
        {
            Console.ForegroundColor = BorderColor;
            Console.BackgroundColor = BackgroundColor;
            for(int y = 0; y < Size.Y; y++)
            {
                for(int x = 0; x < Size.X; x++)
                {
                    Console.SetCursorPosition(Position.X + x, Position.Y + y);
                    if(x == 0 || x == Size.X - 1)
                    {
                        Console.Write("█");
                        continue;
                    }
                    if (y == 0)
                    {
                        Console.Write("▀");
                        continue;
                    }
                    if (y == Size.Y - 1)
                    {
                        Console.Write("▄");
                        continue;
                    }
                    Console.Write(" ");
                }
            }
        }
    }
}
