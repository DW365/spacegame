﻿using System;
using System.Collections.Generic;

namespace SpaceGame
{
    sealed class Game
    {
        private Level Current { get; set; }
        private static readonly Game instance = new Game();
        private Game()
        {

        }
        public static Game Instance
        {
            get { return instance; }
        }
        public void Start()
        {
            Utility.Tools.ConsoleInit();
            GUI.GraphicalUserInterface.Instance.Start();
        }
        public void Resume()
        {
            Current.Start();
        }
        public void StartLevel(int level)
        {
            GameObjects.Player.Instance.ResetPosition();
            if(level == 1)
            {
                var lvl = new Levels.Level1();
                Current = lvl;
                lvl.Start();
            }
            if (level == 2)
            {
                var lvl = new Levels.Level2();
                Current = lvl;
                lvl.Start();
            }
            if (level == 3)
            {
                var lvl = new Levels.Level3();
                Current = lvl;
                lvl.Start();
            }
            if (level == 4)
            {
                var lvl = new Levels.Level4();
                Current = lvl;
                lvl.Start();
            }
            if (level == 5)
            {
                var lvl = new Levels.Level5();
                Current = lvl;
                lvl.Start();
            }
        }
        public void Exit()
        {
            System.Environment.Exit(0);
        }
    }
}
