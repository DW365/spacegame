﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceGame.GameObjects;

namespace SpaceGame
{
    abstract class Level
    {
        public List<GameObject> Entities { get; set; }
        private bool IsActive { get; set; }
        public int Score { get; private set; }
        private Utility.Timer Delay;

        public Level()
        {
            Player.Instance.Lives = Globals.LIVES;
            Entities = new List<GameObject>();
            IsActive = false;
            Delay = new Utility.Timer(Globals.DELAY);
            Score = 0;
            Add(GameObjects.Player.Instance);
        }
        protected int MaxHealth()
        {
            var maxh = Score / 50;
            return Math.Abs(maxh);
        }

        public void GameOver()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();
            Console.SetCursorPosition(Globals.SCREEN_WIDTH / 2 - 10, Globals.SCREEN_HEIGHT / 2);
            Console.Write("Вы набрали {0} очков", Score);
            Console.SetCursorPosition(Globals.SCREEN_WIDTH / 2 - 10, Globals.SCREEN_HEIGHT / 2 +1);
            Console.Write("Нажмите Enter");
            while(Console.ReadKey(true).Key != ConsoleKey.Enter)
            {}
            GUI.GraphicalUserInterface.Instance.ShowMain();
        }

        public void Add(GameObject obj)
        {
            if (!obj.IsCorrect(obj.Position)) return;
            if(IsActive) obj.Show();
            obj.Parent = this;
            Entities.Add(obj);
        }
        public void AddScore(int score)
        {
            Score += score;
        }
        public void Remove(GameObject obj)
        {
            if (obj is Scorable) AddScore(((Scorable)obj).GetScore());
            Entities.Remove(obj);
        }

        protected abstract void UpdateLogic();
        public void Update()
        {
            if(Delay.Step())
            {
                UpdateLogic();
                GUI.GraphicalUserInterface.Instance.ShowBattleScreen(Score,Player.Instance.Lives);
                for(int i = 0; i < Entities.Count; i++)
                {
                    Entities[i].Update();
                }
            }
            
        }
        public void Start()
        {
            IsActive = true;
            for(;;)
            {
                Update();
            }
        }
    }
}
